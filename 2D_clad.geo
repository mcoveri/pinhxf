// Geom
fuel_r = 0.00418;
clad_r = 0.00475;

// Discretization
nom_mesh_scale = 0.0002;
// N azimuths per quartile
n_theta = 50;
// N r divisions in clad
n_clad_r = 30;

// Points
Point(1) = {0, 0, 0, nom_mesh_scale * 2};
// fuel
Point(2) = {0, fuel_r, 0, nom_mesh_scale};
Point(3) = {-fuel_r, 0, 0, nom_mesh_scale};
Point(4) = {0, -fuel_r, 0, nom_mesh_scale};
Point(5) = {fuel_r, 0, 0, nom_mesh_scale};
// clad
Point(6) = {clad_r, 0, 0, 1.0};
Point(7) = {0, clad_r, 0, 1.0};
Point(8) = {-clad_r, 0, 0, 1.0};
Point(9) = {0, -clad_r, 0, 1.0};

// Arcs and lines
Circle(1) = {5, 1, 2};
Circle(2) = {2, 1, 3};
Circle(3) = {3, 1, 4};
Circle(4) = {4, 1, 5};
Circle(5) = {7, 1, 8};
Circle(6) = {8, 1, 9};
Circle(7) = {9, 1, 6};
Circle(8) = {6, 1, 7};

//
Line(13) = {5, 6};
Line(14) = {2, 7};
Line(15) = {3, 8};
Line(16) = {4, 9};

// Surface defs
Line Loop(25) = {1, 14, -8, -13};
Plane Surface(26) = {25};
Line Loop(27) = {5, -15, -2, 14};
Plane Surface(28) = {27};
Line Loop(29) = {15, 6, -16, -3};
Plane Surface(30) = {29};
Line Loop(31) = {7, -13, -4, 16};
Plane Surface(32) = {31};

// Physical boundaries
Physical Line("outside_surf_1") = {8};
Physical Line("outside_surf_2") = {5};
Physical Line("outside_surf_3") = {6};
Physical Line("outside_surf_4") = {7};
//
Physical Line("inside_surf") = {2, 3, 4, 1};

// Physical regions
Physical Surface("clad") = {28, 30, 32, 26};

// Transfinite lines
// clad R
Transfinite Line {14} = n_clad_r Using Progression 1.0;
Transfinite Line {15} = n_clad_r Using Progression 1.0;
Transfinite Line {16} = n_clad_r Using Progression 1.0;
Transfinite Line {13} = n_clad_r Using Progression 1.0;

// fuel theta
Transfinite Line {1} = n_theta Using Progression 1.0;
Transfinite Line {2} = n_theta Using Progression 1.0;
Transfinite Line {3} = n_theta Using Progression 1.0;
Transfinite Line {4} = n_theta Using Progression 1.0;

// clad theta
Transfinite Line {8} = n_theta Using Progression 1.0;
Transfinite Line {5} = n_theta Using Progression 1.0;
Transfinite Line {6} = n_theta Using Progression 1.0;
Transfinite Line {7} = n_theta Using Progression 1.0;

// Transfinite surfaces

// clad
Transfinite Surface {30};
Transfinite Surface {32};
Transfinite Surface {26};
Transfinite Surface {28};

// clad 2D tri -> quad recomb
Recombine Surface {28};
Recombine Surface {30};
Recombine Surface {32};
Recombine Surface {26};
