from fipy import *
import numpy as np
from six import iteritems
from scipy.interpolate import NearestNDInterpolator


class FipyRegion(object):
    """!
    @brief Internal heat source evaluator.
    """
    def __init__(self, regionDict, matDict):
        """!
        @param regionDict Regions in the mesh
        @param srcDict Internal source specifications
        @param field field on which to impose the source term.
        """
        self.regionDict = regionDict
        self.matDict = matDict

    def buildRegions(self, fieldList):
        self._checkFieldNames()
        for regionName, props in iteritems(self.matDict):
            physicalMaterial = self.matDict[regionName]
            physicalRegion = self.regionDict[regionName]
            for field in fieldList:
                field.setValue(self._evalFun(physicalRegion.mesh.cellCenters,
                                             physicalMaterial.get(field.name, 0.)),
                               where=physicalRegion)

    def _checkFieldNames(self):
        for matName, material in iteritems(self.matDict):
            for propName, propVal in iteritems(material):
                if not propName in ['Sv', 'rho', 'cp', 'k']:
                    print("WARNING: Invalid material property: " + str(propName))

    def _evalFun(self, cellCenters, volSrc):
        """!
        @brief Evaluates material properties as a function of space.
        @note: cellCenters.shape == (N_dim, N_centroids)
        @note: volFunc requires coords with (..., N_dim)
        """
        # Constant material prop: no spatial dependence
        if type(volSrc) is float:
            return volSrc
        # Material prop supplied as {x, y, } input array
        if type(volSrc) is np.ndarray:
            volFunc = self._buildInterpOp(volSrc)
        # Check for custom function input
        elif hasattr(volSrc, "__call__"):
            volFunc = volSrc
        else:
            raise RuntimeError("Invalid volumetric src or material type definition.")
        return volFunc(cellCenters.value.T)


    def _buildInterpOp(self, volData):
        return NearestNDInterpolator(volData[:, :-1],
                                     volData[:, -1])


