#!/usr/bin/python2
from fipy import *
import fipy.solvers as fpsv
from six import iteritems
import copy
import sys
import h5py
import fipyBC as bcs
import fipyRegion as rgs


class hxfSolver(object):
    def __init__(self, mshFile, matDict, bcDict, surfChemDict=None, **kwargs):
        """!
        @brief Generic conduction heat transfer solver using the fipy
        package.
        """
        self.matDict, self.bcDict = matDict, bcDict
        self.boundaryDict, self.regionDict = {}, {}
        self.time = 0
        # Init Temperature
        T0 = kwargs.pop("T0", 1000.)
        # LOAD MESH ##
        self._loadMesh(mshFile)
        # ASSIGN MAT PROPS ##
        self._regionBuild()
        self._boundaryBuild()
        # VAR DEFS ##
        self.T = CellVariable(name="T",
                              mesh=self.mesh,
                              value=T0,
                              hasOld=1)
        # Volumetric heat source
        self.Sv = CellVariable(name="Sv",
                               mesh=self.mesh,
                               value=0.)
        # Thermal conductivity
        self.k = CellVariable(name="k",
                              mesh=self.mesh,
                              value=1.0)
        # density * specific heat
        self.rho = CellVariable(name="rho",
                                mesh=self.mesh,
                                value=1.0)
        self.cp = CellVariable(name="cp",
                               mesh=self.mesh,
                               value=1.0)
        # Assign Materials
        self.regionHandler = rgs.FipyRegion(self.regionDict, self.matDict)
        self._matBuild()
        # Set Eqn
        self._setEqn(kwargs.pop("transient", True))
        # Set solver
        self._defSolver()
        # Setup BC handler
        self.bcHandler = bcs.FipyBCHandler(self.boundaryDict, self.bcDict)

    def _setEqn(self, transient=True):
        self.trans = transient
        self.eqE = TransientTerm(coeff=rho * cp) == DiffusionTermCorrection(coeff=k) + Sv

    def _regionBuild(self):
        for regionName, val in iteritems(self.matDict):
            regionMask = mesh.physicalCells[regionName]
            self.regionDict[regionName] = regionMask

    def _boundaryBuild(self):
        for boundaryName, val in iteritems(self.bcDict):
            boundaryMask = mesh.physicalFaces[boundaryName]
            self.boundaryDict[boundaryName] = boundaryMask

    def _matBuild(self):
        self.regionHandler.buildRegions([self.k, self.Sv, self.rho, self.cp])

    def _loadMesh(self, mshFile):
        raise NotImplementedError

    def _runSurfaceChem(self):
        """!
        @brief Execute all surface chemestry models
        """
        pass

    def _defSolver(self, customSolver=None, **kwargs):
        # default GMRES w/ jacobi precon
        pc = fpsv.JacobiPreconditioner()
        self._mysolver = LinearGMRESSolver(tolerance=1e-9, iterations=500, precon=pc)

    def _step(self, timeStep=100.0, ulax=0.86, resCrit=1e-9, iMax=1000):
        """!
        @brief take a step.
        """
        res, resOld, i = 1e10, 0, 0
        while res > resCrit and i < iMax:
            print("Inner= %d, Residual Norm = %f, |T| = %f" % (i, res, max(T)))
            res = self.eqE.sweep(var=self.T,
                                 dt=timeStep,
                                 underRelaxation=ulax,
                                 boundaryConditions=self.bcHandler.applyBCs(i + 1),
                                 solver=self._mysolver)
            i += 1
            if abs(resOld - res) < 1e-15:
                break
            resOld = res

    def simulate(self, steps=1, timeStep=100, **kwargs):
        self.time = 0
        for step in range(steps):
            print(" ---------- Time = %f [s] -----------" % time)
            # INNER ITERATIONS
            self.T.updateOld()
            self._step(timeStep, **kwrgs)
        # Advance time
        self.time += timeStep
        print(" -------- Final Time = %f [s] --------" % time)


class hxf2dSolver(hxfSolver):
    def __init__(self, *args, **kwargs):
        super(self, hxf2dSolver).__init__(*args, **kwargs)

    def _loadMesh(self, mshFile):
        self.mesh = Gmsh2D(mshFile)


class hxf3dSolver(hxfSolver):
    def __init__(self, *args, **kwargs):
        super(self, hxf2dSolver).__init__(*args, **kwargs)

    def _loadMesh(self, mshFile):
        self.mesh = Gmsh3D(mshFile)


def test():
    # 160 percent nominal power
    P_in = 66930.4 * 1.6  # [W]
    fuel_r = 0.00418  # [m]
    fuel_H = 3.765    # [m]
    fuel_A = numerix.pi * fuel_r * 2.0 * fuel_H
    fuel_V = numerix.pi * fuel_r ** 2.0 * fuel_H
    Qv_in = P_in / fuel_V  # [W/m^3]

    # materials
    fuelMat = {'k': 5.0,       # W/m^2k
               'rho': 10970.,  # kg/m^3
               'cp': 3.1e2,    # J/kg-K
               'Sv': Qv_in,     # W/m^3
               }
    cladMat = {'k': 21.0,      # W/m^2k
               'rho': 6600.,   # kg/m^3
               'cp': 2.7e2     # J/kg-K
               }
    # {physical volume names: material props, }
    materialDict = {'outer_fuel': fuelMat, 'inner_fuel': fuelMat, 'clad': cladMat}
    # {physical boundary name: bound vals, }
    """
    bcDict = {'outside_surf_1': ['dirichlet', 550.],
              'outside_surf_2': ['dirichlet', 650.],
              'outside_surf_3': ['dirichlet', 550.],
              'outside_surf_4': ['dirichlet', 650.],
              }
    """
    bcDict = {'outside_surf_1': ['h', 1e5, 610.],
              'outside_surf_2': ['h', 9.e5, 610.],
              'outside_surf_3': ['h', 1e5, 610.],
              'outside_surf_4': ['h', 9.e5, 610.],
              }
    meshFile = '../2D_new_fuel.geo'
    test2dSolver = hxf2dSolver(meshFile, materialDict, bcDict)
    test2dSolver.simulate(1, 500)

if __name__ == "__main__":
    test()
