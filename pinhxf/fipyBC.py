from fipy import *


class FipyBCHandler(object):
    """!
    @brief External boundary condition storage and evaluator.
    Contains handlers for constant and func(x,y,z)
    bc of type neumann and diriclet.
    """
    def __init__(self, boundaryDict, bcDict, field=None):
        """!
        @brief Initilize boundary condition handler.
        @param boundaryDict  Mesh-boundary name specifications
        @param bcDict  Boundary condition specifications
        """
        self.boundaryDict = boundaryDict
        self.bcDict = bcDict
        self.field = field

    def _assignBCs(self):
        bcList = []
        for boundaryName, boundaryCond in iteritems(self.bcDict):
            bcTypeStr = boundaryCond[0]
            typeBc = boundaryCond[0]

    def _handler(self, faceCells, boundaryCond):
        """!
        @brief Applies boundary conditions to face Cells.
        @param faceCells  Fipy FaceVariable
        @param boundaryCond  ["name": (vals, )]
        """
        if boundaryCond[0] is 'h':
            vals = self._eval(boundaryCond[1], faceCells)
            return FixedFlux(faceCells, vals)
        elif boundaryCond[0] is 'q':
            vals = self._eval(boundaryCond[1], faceCells)
            return FixedFlux(faceCells, vals)
        elif boundaryCond[0] is 'dirichlet':
            vals = boundaryCond[1]
            return FixedValue(faceCells, vals)
        else:
            raise RuntimeError("Invalid BC")

    def applyBCs(self, i=None):
        """!
        @brief Returns a tuple of Fipy boundary conditions
        """
        bcList = []
        for boundaryName, boundaryCond in iteritems(self.bcDict):
            faceCells = self.boundaryDict[boundaryName]
            bcTypeStr = boundaryCond[0]
            assert(type(bcTypeStr) is str)
            bcList.append(self._handler(faceCells, boundaryCond))
        return tuple(bcList)


class Bcond(object):
    def __init__(self):
        pass

    def eval(self, boundaryCond):
        """!
        @brief Inspect the type of the boundary condition,
        if given as a function, run spatial interpolation routine,
        otherwise simply apply the constant.
        """
        assert(type(boundaryCond) is list)
        assert(type(boundaryCond[1]) is tuple)
        if type(boundaryCond[1][0]) is int or type(boundaryCond[1][0]) is float:
            return self._evalConst()
        else:
            return self._evalSpatialFn()

    def _evalSpatialFn(self):
        """!
        @brief If the boundary condition contains a fun: f(x, y, z),
        eval f(x,y,z) at the face centroids
        """
        interpVals = self.boundaryCond

    def _evalConst(self):
        """!
        @brief If the boundary condition is specified as a const,
        return the value of the constant
        """
        return self.boundaryCond[1]

    def _evalCond(self):
        raise NotImplementedError


class NeumannBC(Bcond):
    def __init__(self):
        pass

    def _evalCond(self):
        return - q

class NewtonsCoolBC(Bcond):
    def __init__(self):
        pass

    def _evalCond(self):
        return h * (T.faceValue - Tc)
