#!/usr/bin/python2
from fipy import *
import fipy.solvers as fpsv
from six import iteritems
import copy
import sys
import h5py
import pylab as pl
import seaborn as sns


# 160 percent nominal power
P_in = 66930.4 * 1.6  # [W]
fuel_r = 0.00418  # [m]
fuel_H = 3.765    # [m]
fuel_A = numerix.pi * fuel_r * 2.0 * fuel_H
fuel_V = numerix.pi * fuel_r ** 2.0 * fuel_H
Qv_in = P_in / fuel_V  # [W/m^3]

#################
# Material data #
#################
fuelMat = {'k': 5.0,       # W/m^2k
           'rho': 10970.,  # kg/m^3
           'cp': 3.1e2,    # J/kg-K
           'Sv': Qv_in,     # W/m^3
           }
cladMat = {'k': 21.0,      # W/m^2k
           'rho': 6600.,   # kg/m^3
           'cp': 2.7e2     # J/kg-K
           }
# {physical volume names: material props, }
materialDict = {'outer_fuel': fuelMat, 'inner_fuel': fuelMat, 'clad': cladMat}

# {physical boundary name: bound vals, }
"""
bcDict = {'outside_surf_1': ['dirichlet', 550.],
          'outside_surf_2': ['dirichlet', 650.],
          'outside_surf_3': ['dirichlet', 550.],
          'outside_surf_4': ['dirichlet', 650.],
          }
"""
bcDict = {'outside_surf_1': ['h', 9e5, 610.],
          'outside_surf_2': ['h', 1e5, 610.],
          'outside_surf_3': ['h', 9e5, 610.],
          'outside_surf_4': ['h', 1e5, 610.],
          }

#################
# Read the Mesh #
#################
mesh = Gmsh2D('2D_new_fuel.geo')
boundaryDict, regionDict = {}, {}

for regionName, val in iteritems(materialDict):
    regionMask = mesh.physicalCells[regionName]
    regionDict[regionName] = regionMask

for boundaryName, val in iteritems(bcDict):
    boundaryMask = mesh.physicalFaces[boundaryName]
    boundaryDict[boundaryName] = boundaryMask

# Obtain locations of cell centers and edges
# rc = mesh.cellCenters
# rfc = mesh.faceCenters
# dys = mesh.scaledCellToCellDistances

###############
#  Field Vars #
###############
T0 = 800.
# Temperature
T = CellVariable(name="T",
                 mesh=mesh,
                 value=T0,
                 hasOld=1)
# Volumetric heat source
Sv = CellVariable(name="Sv",
                  mesh=mesh,
                  value=0.)
# Thermal conductivity
k = CellVariable(name="k",
                 mesh=mesh,
                 value=1.0)
# density * specific heat
rhoCp = CellVariable(name="rhoCp",
                     mesh=mesh,
                     value=1.0)

######################
# Set material props #
######################
for regionName, props in iteritems(materialDict):
    physicalMaterial = materialDict[regionName]
    physicalRegion = regionDict[regionName]
    Sv.setValue(physicalMaterial.get('Sv', 0.), where=physicalRegion)
    k.setValue(physicalMaterial['k'], where=physicalRegion)
    rhoCp.setValue(physicalMaterial['rho'] * physicalMaterial['cp'],
                   where=physicalRegion)


##################
# Boundary Conds #
##################
def computeDTdr(h, Tc, faceCells):
    """!
    @brief Newton's law of cooling.
    \f$ \frac{dQ}{dx} = h A (T(x) - Tc) = -k\frac{dT}{dx} \f$
    """
    # dTdr = - h * (T.arithmeticFaceValue[faceCells] - Tc) / k.arithmeticFaceValue[faceCells]
    currentDTdr = (-h * (T.faceValue[faceCells] - Tc) / k.faceValue[faceCells])
    return currentDTdr

def computeDTdrQ(q, faceCells):
    return - q / k.faceValue[faceCells]


def boundaryCondHandler(faceCells, boundaryCond, field=T):
    """!
    @brief Applies boundary conditions to face Cells
    """
    if boundaryCond[0] is 'neumann':
        field.faceGrad.constrain(boundaryCond[1] * mesh.faceNormals[:, faceCells],
                                 where=faceCells)
    elif boundaryCond[0] is 'h':
        field.faceGrad.constrain(computeDTdr(boundaryCond[1],
                                             boundaryCond[2],
                                             faceCells).value *
                                 mesh.faceNormals[:, faceCells],
                                 where=faceCells)
    elif boundaryCond[0] is 'q':
        field.faceGrad.constrain(computeDTdrQ(boundaryCond[1],
                                              faceCells).value *
                                 mesh.faceNormals[:, faceCells],
                                 where=faceCells)
    elif boundaryCond[0] is 'dirichlet':
        field.constrain(boundaryCond[1],
                        where=faceCells)
    else:
        sys.exit("Invalid BC")

def applyBCs(iteration=0):
    for boundaryName, boundaryCond in iteritems(bcDict):
        if iteration >= 1 and boundaryCond[0] is not 'h':
            continue
        faceCells = boundaryDict[boundaryName].value
        boundaryCondHandler(faceCells, boundaryCond)
applyBCs()

##################
# Energy Balance #
##################
eqE = TransientTerm(coeff=rhoCp) == DiffusionTerm(coeff=k) + Sv

#####################
# Linear Sys Solver #
#####################
#pc = fpsv.JacobiPreconditioner()
# mysolver = LinearGMRESSolver(tolerance=1e-6, iterations=300, precon=pc)
mysolver = LinearPCGSolver()

################
# Step control #
################
timeStep = 1000             # [seconds]
steps = 1                  # total number of tsteps
res, resOld = 1.0e10, 1.0   # residual storage
resCrit = 1e-10              # residual thresholds
i, iMax = 0, 2000             # inner iteration control
ulax = 0.86
tStore, tT = [0.2, 0.4], {}

###########
# Stepper #
###########
time = 0
for step in range(steps):
    print(" ---------- Time = %f [s] -----------" % time)
    # INNER ITERATIONS
    while res > resCrit and i < iMax:
        # Update BC's
        res = eqE.sweep(var=T,
                        dt=timeStep,
                        underRelaxation=ulax,
                        solver=mysolver)
        if i > 0:
            print("Inner = %d, Residual Norm = %e, |T| = %f" % (i, res, max(T)))
        i += 1
        if abs(resOld - res) < 1e-15:
            break
        resOld = res
        applyBCs(step + 1)
    T.updateOld()
    res, i = 1.0e10, 0
    # Store solution at indicated times for plotting later
    if time in tStore:
        tT[time] = copy.deepcopy(T)
    # Advance time
    time += timeStep
print(" -------- Final Time = %f [s] --------" % time)


###############
# Export Data #
###############
interiorCladCells = mesh.physicalFaces["inside_surf"].value
exteriorCladCells = (mesh.physicalFaces["outside_surf_1"].value |
                     mesh.physicalFaces["outside_surf_2"].value |
                     mesh.physicalFaces["outside_surf_3"].value |
                     mesh.physicalFaces["outside_surf_4"].value)
# Temps
Tin = T.faceValue.value[interiorCladCells]
Tout = T.faceValue.value[exteriorCladCells]
#Tgrad_in = T.faceGrad.value[interiorCladCells]
#Tgrad_out = T.faceGrad.value[exteriorCladCells]
# Coords
xin = mesh.faceCenters.value[:, interiorCladCells]
xout = mesh.faceCenters.value[:, exteriorCladCells]
# Convert x,y to (r, theta)
r = numerix.linalg.norm(xout, axis=0)
theta = numerix.arctan2(xout[1, :], xout[0, :])
t_theta = numerix.array([theta, Tout]).T
t_theta = t_theta[t_theta[:, 0].argsort()]
h5f = h5py.File("fuel_clad.h5", 'w')
h5f.create_group("inside_surf")
h5f.create_group("outside_surf")
h5f.create_dataset("outside_surf/r_T", data=t_theta)
h5f.close()

#############
# View Data #
#############
pl.figure(10)
pl.plot(t_theta[:, 0], t_theta[:, 1], label="Fuel - Outer Surface T")
pl.legend()
pl.xlabel("Azimuth [rad]")
pl.ylabel("Temperature [K]")
pl.savefig("outer_T_fuel.png")

viewer = Viewer(vars=T)
viewer.plot("2D_fuel_clad.png")
raw_input("Press <return> to exit...")
