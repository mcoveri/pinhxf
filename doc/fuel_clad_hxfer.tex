\documentclass[12pt,a4paper]{report}
\usepackage[a4paper, total={6.25in, 9in}]{geometry}
\usepackage{setspace}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{multicol}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{subfig}
%
%
\author{William Gurecky}
\title{CFD Informed Subchannel}
\begin{document}

%-----------------------------------------------------------------------------%
\begin{titlepage}
	\centering
	{\scshape\LARGE The Consortium for the Advanced Simulation of Light Water Reactors \par}
	\vspace{1.5 cm}
	{\scshape\LARGE  \par}
	\vspace{1.5cm}
	{\huge\bfseries Pin Cell Heat Conduction Study \par}
	\vspace{2cm}
	{\Large The University of Texas at Austin \par}
	\vfill

	\begin{flushright}
	~\textsc{Authors}: \par
	\bigskip
	William Gurecky  \par
	\end{flushright}
	\vfill
	{\large \today\par}
\end{titlepage}
%-----------------------------------------------------------------------------%
\pagebreak
\tableofcontents
\pagebreak
%-----------------------------------------------------------------------------%
\section*{Acronyms}
\begin{tabular}{l l}
CASL & Consortium for the Advanced Simulation of LWRs \\
CFD &  Computational Fluid Dynamics \\
CILC & CRUD Induced Local Corrosion \\
CIPS & CRUD Induced Power Shift \\
CRUD & Chalk River Unidentified Deposit \\
CTF &  Cobra-Two Fluid (Cobra-TF) \\
TH  &  Thermal Hydraulic \\
VERA & Virtual Environment for Reactor Applications \\
\end{tabular}

\section*{Symbols}
\begin{tabular}{l l}
t & Time \\
T & Temperature \\
$C_p$ & Isobaric specific heat \\
$\rho$ & Density \\
$k$ & Thermal conductivity \\
$S_v$  & Volumetric heat source \\
$q''$ &  Heat flux \\
$h$ & Heat transfer coefficient \\
\end{tabular}

\pagebreak
\onehalfspacing

%-----------------------------------------------------------------------------%
\chapter{Pin Cell Heat Transfer}

Here, the impact of neglecting conductive heat transfer within the fuel and gap regions on coolant-cladding interface temperatures is investigated.  It is of interest to model conductive heat transfer only in the cladding in order to reduce computational requirements.  It is desirable to reduce the runtime of the conduction solve performed as part of a clad CILC tool.  Such a tool is currently under development in the VERA package Cicada.  The cladding heat transfer solver, surface oxide model, and CRUD growth model are collected in Cicada and are coupled with STAR-CCM+ through STAR's C API.

\section{Model Definition}

A simplified 2D ($(r, \theta)$) pin is considered in this investigation.  Only the fuel and cladding regions are considered, the gap is neglected. The conductive heat transport within the fuel and cladding is described by the heat equation:

\begin{equation}
\rho C_p \frac{\partial T}{\partial t} = \nabla \cdot( k \nabla T) + S_v
\label{heatEq}
\end{equation}

The primary mode of heat transport on the outside surface of the cladding is convection which is described by Newtons law of cooling provided in equation \ref{newtonCool}.  Therefore, a Neumann boundary condition is applied on the coolant-cladding interface according to equation \ref{Neumann}.

\begin{equation}
q'' = -k\nabla T = h(\Delta T)
\label{newtonCool}
\end{equation}

\begin{equation}
\frac{\partial T}{\partial (\bf{r} \cdot \hat{n})}\bigg\vert_{S_o} = \frac{-h}{k} (T - T_{\infty})  \bigg\vert_{S_o}
\label{Neumann}
\end{equation}
With $S_o$ denoting the cladding outer surface with an outward normal $\mathbf{\hat{n}}$. 

The numerical model of equation \ref{heatEq} was constructed via the software package Fipy [\url{http://www.ctcms.nist.gov/fipy/}].  Fipy excels as a finite volume PDE toolkit for unstructured Cartesian meshes.  Briefly, Fipy applies the following approximations to equation \ref{heatEq}:

\begin{equation}
\int_V \nabla \cdot (k \nabla T) dV = \int_S  k (\hat{n} \cdot \nabla T)dS
\end{equation}

At an interface, $f_i$, between FV cells (with cell centres at $A$ and $B$) the flux is approximated according to:

\begin{equation}
\int_S  k (\hat{n} \cdot \nabla T)dS \approx \sum_{i=1}^M A_{f_i} k_{f_i} \frac{T^{t+1}_A - T^{t+1}_B}{d_{AB}}
\end{equation}
 $d_{AB}$ is the distance between cell centers, $M$ is the number of adjacent cells, and $t+1$ is the time at the current iteration.  The flux approximation becomes less accurate if the neighbouring cells are non-orthogonal. This is a known weakness of the Fipy package since it employs a cell-centred FV scheme without a non-orthogonality correction term.

The time derivative is represented by the following formulation:

\begin{equation}
    \int_V \frac{\partial (\rho C_p T)}{\partial t}dV \approx V \rho C_p \frac{(T)^{t+1} - (T)^{t}}{\Delta t}
\end{equation}
And $T = T(\mathbf{r}, t)$ is the solution variable.

An implicit time marching scheme was used according to:

\begin{equation}
T^{t+1} = T^{t} - \Delta t \nabla_t T^{t+1}
\end{equation}

Which must be solved for the new solution vector, $T^{t+1}$ at every time step.

\begin{equation}
    \nabla_t T^{t+1} = \frac{(T)^{t+1} - (T)^{t}}{\Delta t} =  L (T^{t+1})
\end{equation}

Where $L(\cdot)$ is the diffusion operator.
\begin{equation}
    \nabla_t
\end{equation}

\subsection{Geometry}

The nominal pin geometry and material properties are given in table \ref{modelGeom}. The mesh parameters are given in table \ref{meshProp}. The unstructured Cartesian mesh was generated by the GMSH package [\url{http://gmsh.info/}].

\begin{table}[h]
\begin{center}
  \begin{tabular}{ | l || c |}
    \hline
    Rod height & 365.76 [cm] \\ \hline
    Clad outer radius & 0.475 [cm] \\ \hline 
    Clad inner radius & 0.425 [cm] \\ \hline 
    $C_p$ fuel  &  3.1e2$[J/kgK]$ \\ \hline 
     $C_p$ clad  &  2.7e2 $[J/kgK]$ \\ \hline 
     $k$ fuel  &  5.0 $[W/m^2K]$ \\ \hline 
     $k$ clad  &  21.0 $[W/m^2K]$ \\ \hline 
     $\rho$ fuel  &  10970 $[kg/m^3]$ \\ \hline 
     $\rho$ clad  &  6600 $[kg/m^3]$ \\ \hline 

  \end{tabular}
\end{center}
\caption{Pin geometry and material properties.}
\label{modelGeom}
\end{table}

\begin{table}[h]
\begin{center}
  \begin{tabular}{ | l || c |}
    \hline
    Cell Type & quadrilateral \\ \hline
    $N_{\theta_{quad}}$ & 50 $[\theta/quadrent]$ \\ \hline
    $N_{clad_R}$ & 30   \\ \hline
    $N_{fuel_R}$ & 50   \\ \hline
  \end{tabular}
\end{center}
\caption{Mesh parameters.}
\label{meshProp}
\end{table}

\section{Model Results}

\subsection{Heat Transfer Coefficient Sweep}

The thermal conditions are provided in table \ref{modelTH} for each case in the study.  The heat transfer coefficient on the outer surface of the pin was set in a 90 degree periodic fashion, alternating between $h_1$ and $h_2$ given in table \ref{modelTH}.  Variation in the heat transfer coefficient represents the effect of grid vane induced mixing, giving rise to hot and cold spots on the outer pin surface.  Realistic quantities for the local heat transfer variation on a pin's surface downstream of a spacer grid can be ascertained from CFD calculations.  Figure \ref{cfd_h} displays the variation in the local heat transfer coefficient for a rod with an approximately uniform heating rate of $4.476718e8 [W/m^3]$.  The largest local variation in convective heat transfer rates occurs downstream of the spacer grids. \\

\begin{figure}[h!]
\centering
\includegraphics[scale=.4]{images/cfd_h_local.png}
\caption{CFD estimated local heat transfer coefficient, $h\ [W/m^2 K]$.}
\label{cfd_h}
\end{figure}
\begin{figure}[h!]
\centering
\includegraphics[scale=.4]{images/cfd_t_ref.png}
\caption{CFD estimated reference temperature, $T_\infty$, for newtons law of cooling.}
\label{cfd_tref}
\end{figure}

It is hypothesized that increasing the local variation (to extreme values) of the heat transfer coefficient will increase the relative importance of azimuthal heat transfer in the fuel region.  However, under normal operating conditions and under a uniform internal volumetric heat source, a negligible amount of azimuthal heat redistribution is expected in the fuel region since the thermal conductivity is 5 times larger in the cladding than in the fuel and the heat source is spread evenly throughout the fuel volume.  If the heat source was concentrated at a single point at the center of the fuel, azimuthal conduction in the fuel region would be substantially more important with respect to predicting the outer-pin surface temperature.
Relative to the cladding there exists a very small effective azimuthal conduction pathway in the fuel. \\

For each case, the model was first executed with the fuel region included. A uniform volumetric heat source was applied in the fuel region with a value: $518.174e6\ [W/m^3]$.  This was followed by a calculation performed without the fuel region present. When the fuel region was removed from the model, a constant heat flux boundary condition was applied to the interior surface of the clad with a value: $1.082985e8\ [W/m^2]$.  The maximum outer surface temperature disagreements between the two models are tabulated in right-most column in table \ref{modelTH}.

\begin{table}[h]
\begin{center}
  \begin{tabular}{ | c || c | c | c | c | c |}
    \hline
    $case$ & $h_1$ $[W/m^2K]$ & $h_2$ $[W/m^2K]$  & $T_{\infty} [K]$ & $P$ $[W]$ & $max(|\Delta T_{S_o}|) [K]$ \\ \hline \hline
    1 & 1.0e5  & 9.0e5 & 610. & 107089 &  0.0840 \\ \hline
    2 & 1.0e5  & 4.5e5 & $\downarrow$  & $\downarrow$ &  0.0708 \\ \hline
    3 & 1.0e5  & 2.0e5 & $\downarrow$  & $\downarrow$ &  0.0390 \\ \hline
    4 & 1.0e4  & 1.0e5 & $\downarrow$  & $\downarrow$ &  6.704 \\ \hline
    5 & 1.0e3  & 1.0e5 & $\downarrow$  & $\downarrow$ &  119.200 \\ \hline
  \end{tabular}
\end{center}
\caption{Heat transfer coefficient specification and maximum outer surface difference.}
\label{modelTH}
\end{table}

\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[scale=.6]{images/2D_fuel_clad_1e5_45e5.png}
    \caption{Cladding $+$ fuel model. Case 2 result.}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[scale=.6]{images/2D_clad_only_1e5_45e5.png}
    \caption{Cladding only model. Case 2 result.}
  \end{minipage}
\end{figure}

A comparison of the outer surface temperature predictions between models are given in figures \ref{result1} - \ref{result5}.  Cases 1-3 represent realistic heat transfer coefficient variation downstream of a spacer grid.  The results confirm that for typical spatial variation in surface heat transfer, a \emph{uniform internal volumetric heat source} can be replaced by a uniform heat flux boundary condition applied to the interior surface of the cladding with minimal impact ($\approx \pm 0.1[K]$) on the exterior cladding surface temperature.  \\

Cases 4 and 5 represent unrealistic scenarios with extreme azimuthal variation in the surface transfer coefficient.  The case 5 result is illustrated in figures \ref{5_fuel} and \ref{5_clad}. In case 5, half of the outer surface is nearly adiabatic thus redirecting the vast majority of the energy out of the two strongly convecting surfaces on opposing sides of the pin. In these cases, the inclusion of the fuel region significantly effects the maximum outer surface temperature. \\

\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[scale=.6]{images/case_6.png}
    \caption{Cladding $+$ fuel model. Case 5 result.}
    \label{5_fuel}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[scale=.6]{images/case_6_clad.png}
    \caption{Cladding only model. Case 5 result.}
    \label{5_clad}
  \end{minipage}
\end{figure}


\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{images/model_compare_1e5_9e5.png}
\caption{Case 1: Temperature $[K]$ vs Azimuth $[rad]$. (blue) Cladding-only model.  (green) Cladding and fuel region model.  (red) Absolute difference.}
\label{result1}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{images/model_compare_1e5_45e5.png}
\caption{Case 2: Temperature $[K]$ vs Azimuth $[rad]$. (blue) Cladding-only model.  (green) Cladding and fuel region model.  (red) Absolute difference.}
\label{result2}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{images/model_compare_1e5_2e5.png}
\caption{Case 3: Temperature $[K]$ vs Azimuth $[rad]$. (blue) Cladding-only model.  (green) Cladding and fuel region model.  (red) Absolute difference.}
\label{result3}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{images/model_compare_1e4_1e5.png}
\caption{Case 4: Temperature $[K]$ vs Azimuth $[rad]$. (blue) Cladding-only model.  (green) Cladding and fuel region model.  (red) Absolute difference.}
\label{result4}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{images/model_compare_1e3_1e5.png}
\caption{Case 5: Temperature $[K]$ vs Azimuth $[rad]$. (blue) Cladding-only model.  (green) Cladding and fuel region model.  (red) Absolute difference.}
\label{result5}
\end{figure}

\pagebreak
\subsection{Fuel Thermal Conductivity Study}

Case 1 was repeated with a fuel thermal conductivity of $21 [W/m^2-K]$.  The comparison between the cladding only model and the highly-conductive fuel-inclusive model is provided in figure \ref{result1b}.  The maximum outer surface temperature difference is $0.2759[K]$.

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{images/model_compare_1e5_9e5_b.png}
\caption{Case 1*:  *Fuel conductivity $=21 [W/m^2-K]$. Temperature $[K]$ vs Azimuth $[rad]$. (blue) Cladding-only model.  (green) Cladding and fuel region model.  (red) Absolute difference.}
\label{result1b}
\end{figure}


%-----------------------------------------------------------------------------%
\end{document}
