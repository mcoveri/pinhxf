// Geom
fuel_r = 0.00418;
clad_r = 0.00475;

// Discretization
nom_mesh_scale = 0.0015;
// N azimuths per quartile
n_theta = 50;
// N r divisions in clad
n_clad_r = 30;

// Centroid
Point(1) = {0, 0, 0, nom_mesh_scale};

// fuel
Point(2) = {fuel_r, 0, 0, 1.0};
Point(3) = {0, fuel_r, 0, 1.0};
Point(4) = {-fuel_r, 0, 0, 1.0};
Point(5) = {0, -fuel_r, 0, 1.0};

// clad
Point(6) = {clad_r, 0, 0, 1.0};
Point(7) = {0, clad_r, 0, 1.0};
Point(8) = {-clad_r, 0, 0, 1.0};
Point(9) = {0, -clad_r, 0, 1.0};

// inner fuel
Point(10) = {fuel_r / 3.0, 0, 0, 1.0};
Point(11) = {0, fuel_r / 3.0, 0, 1.0};
Point(12) = {-fuel_r / 3.0, 0, 0, 1.0};
Point(13) = {0, -fuel_r / 3.0, 0, 1.0};

// lines
Circle(1) = {10, 1, 11};
Circle(2) = {11, 1, 12};
Circle(3) = {12, 1, 13};
Circle(4) = {13, 1, 10};
Circle(5) = {2, 1, 3};
Circle(6) = {3, 1, 4};
Circle(7) = {4, 1, 5};
Circle(8) = {5, 1, 2};
Circle(9) = {6, 1, 7};
Circle(10) = {7, 1, 8};
Circle(11) = {8, 1, 9};
Circle(12) = {9, 1, 6};
Line(13) = {1, 11};
Line(14) = {11, 3};
Line(15) = {3, 7};
Line(16) = {1, 12};
Line(17) = {12, 4};
Line(18) = {4, 8};
Line(19) = {1, 13};
Line(20) = {13, 5};
Line(21) = {5, 9};
Line(22) = {1, 10};
Line(23) = {10, 2};
Line(24) = {2, 6};
Line Loop(25) = {2, 3, 4, 1};
Plane Surface(26) = {25};
Line Loop(27) = {14, 6, -17, -2};
Plane Surface(28) = {27};
Line Loop(29) = {17, 7, -20, -3};
Plane Surface(30) = {29};
Line Loop(31) = {20, 8, -23, -4};
Plane Surface(32) = {31};
Line Loop(33) = {23, 5, -14, -1};
Plane Surface(34) = {33};
Line Loop(35) = {6, 18, -10, -15};
Plane Surface(36) = {35};
Line Loop(37) = {18, 11, -21, -7};
Plane Surface(38) = {37};
Line Loop(39) = {12, -24, -8, 21};
Plane Surface(40) = {39};
Line Loop(41) = {5, 15, -9, -24};
Plane Surface(42) = {41};

// physical groups
Physical Line("outside_surf_1") = {9};
Physical Line("outside_surf_2") = {10};
Physical Line("outside_surf_3") = {11};
Physical Line("outside_surf_4") = {12};
Physical Line("inside_surf") = {5, 6, 7, 8};
Physical Surface("clad") = {36, 38, 40, 42};
Physical Surface("outer_fuel") = {28, 30, 32, 34};
Physical Surface("inner_fuel") = {26};

// structured mesh directives
Transfinite Line {9} = n_theta Using Progression 1;
Transfinite Line {10} = n_theta Using Progression 1;
Transfinite Line {11} = n_theta Using Progression 1;
Transfinite Line {12} = n_theta Using Progression 1;
Transfinite Line {5} = n_theta Using Progression 1;
Transfinite Line {6} = n_theta Using Progression 1;
Transfinite Line {7} = n_theta Using Progression 1;
Transfinite Line {8} = n_theta Using Progression 1;
//
Transfinite Line {15} = n_clad_r Using Progression 1;
Transfinite Line {18} = n_clad_r Using Progression 1;
Transfinite Line {21} = n_clad_r Using Progression 1;
Transfinite Line {24} = n_clad_r Using Progression 1;
//
Transfinite Line {14} = n_theta Using Progression 1;
Transfinite Line {17} = n_theta Using Progression 1;
Transfinite Line {20} = n_theta Using Progression 1;
Transfinite Line {23} = n_theta Using Progression 1;
Transfinite Surface {36};
Transfinite Surface {38};
Transfinite Surface {40};
Transfinite Surface {42};
Transfinite Line {1} = n_theta Using Progression 1;
Transfinite Line {2} = n_theta Using Progression 1;
Transfinite Line {3} = n_theta Using Progression 1;
Transfinite Line {4} = n_theta Using Progression 1;
Transfinite Surface {34};
Transfinite Surface {28};
Transfinite Surface {30};
Transfinite Surface {32};

// tris -> quads
Recombine Surface {36};
Recombine Surface {38};
Recombine Surface {40};
Recombine Surface {42};
Recombine Surface {34};
Recombine Surface {28};
Recombine Surface {30};
Recombine Surface {32};
Recombine Surface {26};
