#!/usr/bin/python2
from fipy import *
import fipy.solvers as fpsv
from six import iteritems
import copy
import sys
import h5py
import pylab as pl
import seaborn as sns


# 160 percent nominal power
P_in = 66930.4 * 1.6  # [W]
fuel_r = 0.00418  # [m]
fuel_H = 3.765    # [m]
fuel_A = numerix.pi * fuel_r * 2.0 * fuel_H
fuel_V = numerix.pi * fuel_r ** 2.0 * fuel_H
Qv_in = P_in / fuel_V  # [W/m^3]

#################
# Material data #
#################
fuelMat = {'k': 21.0,       # W/m^2k
           'rho': 10970.,  # kg/m^3
           'cp': 3.1e2,    # J/kg-K
           'Sv': Qv_in,     # W/m^3
           }
cladMat = {'k': 21.0,      # W/m^2k
           'rho': 6600.,   # kg/m^3
           'cp': 2.7e2     # J/kg-K
           }
# {physical volume names: material props, }
materialDict = {'outer_fuel': fuelMat, 'inner_fuel': fuelMat, 'clad': cladMat}

# {physical boundary name: bound vals, }
"""
bcDict = {'outside_surf_1': ['dirichlet', 550.],
          'outside_surf_2': ['dirichlet', 650.],
          'outside_surf_3': ['dirichlet', 550.],
          'outside_surf_4': ['dirichlet', 650.],
          }
"""
bcDict = {'outside_surf_1': ['h', 9.0e5, 610.],
          'outside_surf_2': ['h', 1.0e5, 610.],
          'outside_surf_3': ['h', 9.0e5, 610.],
          'outside_surf_4': ['h', 1.0e5, 610.],
          }

#################
# Read the Mesh #
#################
mesh = Gmsh2D('2D_new_fuel.geo')
boundaryDict, regionDict = {}, {}

for regionName, val in iteritems(materialDict):
    regionMask = mesh.physicalCells[regionName]
    regionDict[regionName] = regionMask

for boundaryName, val in iteritems(bcDict):
    boundaryMask = mesh.physicalFaces[boundaryName]
    boundaryDict[boundaryName] = boundaryMask

###############
#  Field Vars #
###############
T0 = 1000.
# Temperature
T = CellVariable(name="T",
                 mesh=mesh,
                 value=T0,
                 hasOld=1)
# Volumetric heat source
Sv = CellVariable(name="Sv",
                  mesh=mesh,
                  value=0.)
# Thermal conductivity
k = CellVariable(name="k",
                 mesh=mesh,
                 value=1.0)
# density * specific heat
rhoCp = CellVariable(name="rhoCp",
                     mesh=mesh,
                     value=1.0)

######################
# Set material props #
######################
for regionName, props in iteritems(materialDict):
    physicalMaterial = materialDict[regionName]
    physicalRegion = regionDict[regionName]
    Sv.setValue(physicalMaterial.get('Sv', 0.), where=physicalRegion)
    k.setValue(physicalMaterial['k'], where=physicalRegion)
    rhoCp.setValue(physicalMaterial['rho'] * physicalMaterial['cp'],
                   where=physicalRegion)


##################
# Boundary Conds #
##################
def computeDTdr(h, Tc, faceCells):
    """!
    @brief Newton's law of cooling.
    \f$ \frac{dQ}{dx} = h A (T(x) - Tc) = -k\frac{dT}{dx} \f$
    """
    dTdr = h * (T.faceValue - Tc)
    return dTdr


def computeDTdrQ(q, faceCells):
    return - q


def boundaryCondHandler(faceCells, boundaryCond, field=T):
    """!
    @brief Applies boundary conditions to face Cells
    """
    if boundaryCond[0] is 'neumann':
        vals = boundaryCond[1]
        return FixedFlux(faceCells, vals)
    elif boundaryCond[0] is 'h':
        vals = computeDTdr(boundaryCond[1], boundaryCond[2], faceCells)
        return FixedFlux(faceCells, vals)
    elif boundaryCond[0] is 'q':
        vals = computeDTdrQ(boundaryCond[1], faceCells)
        return FixedFlux(faceCells, vals)
    elif boundaryCond[0] is 'dirichlet':
        field.constrain(boundaryCond[1],
                        where=faceCells)
    else:
        sys.exit("Invalid BC")


def applyBCs(iteration=0):
    bcList = []
    for boundaryName, boundaryCond in iteritems(bcDict):
        faceCells = boundaryDict[boundaryName]
        bcList.append(boundaryCondHandler(faceCells, boundaryCond))
    return tuple(bcList)

##################
# Energy Balance #
##################
eqE = TransientTerm(coeff=rhoCp) == DiffusionTermCorrection(coeff=k) + Sv

#####################
# Linear Sys Solver #
#####################
pc = fpsv.JacobiPreconditioner()
mysolver = LinearGMRESSolver(tolerance=1e-9, iterations=500, precon=pc)

################
# Step control #
################
timeStep = 500.0              # [seconds]
steps = 1                     # total number of tsteps
res, resOld = 1.0e10, 1.0     # residual storage
resCrit = 1e-8                # residual thresholds
i, iMax = 0, 10000             # inner iteration control
ulax = 0.86
tStore, tT = [0.2, 0.4], {}

###########
# Stepper #
###########
time = 0
for step in range(steps):
    print(" ---------- Time = %f [s] -----------" % time)
    # INNER ITERATIONS
    T.updateOld()
    while res > resCrit and i < iMax:
        eqE.cacheRHSvector()
        res = eqE.sweep(var=T,
                        dt=timeStep,
                        underRelaxation=ulax,
                        boundaryConditions=applyBCs(i + 1),
                        solver=mysolver)
        res = res / numerix.linalg.norm(eqE.RHSvector)
        print("Inner= %d, Residual Norm = %e, |T| = %f" % (i, res, max(T)))
        i += 1
        if abs(resOld - res) < 1e-15:
            # No change in residual :( stuck.
            break
        resOld = res
        # Update BC's
        # applyBCs(step + 1)
    res, i = 1.0e10, 0
    # Store solution at indicated times for plotting later
    if time in tStore:
        tT[time] = copy.deepcopy(T)
    # Advance time
    time += timeStep
print(" -------- Final Time = %f [s] --------" % time)


###############
# Export Data #
###############
interiorCladCells = mesh.physicalFaces["inside_surf"].value
exteriorCladCells = (mesh.physicalFaces["outside_surf_1"].value |
                     mesh.physicalFaces["outside_surf_2"].value |
                     mesh.physicalFaces["outside_surf_3"].value |
                     mesh.physicalFaces["outside_surf_4"].value)
# Temps
Tin = T.faceValue.value[interiorCladCells]
Tout = T.faceValue.value[exteriorCladCells]
#Tgrad_in = T.faceGrad.value[interiorCladCells]
#Tgrad_out = T.faceGrad.value[exteriorCladCells]
# Coords
xin = mesh.faceCenters.value[:, interiorCladCells]
xout = mesh.faceCenters.value[:, exteriorCladCells]
# Convert x,y to (r, theta)
r = numerix.linalg.norm(xout, axis=0)
theta = numerix.arctan2(xout[1, :], xout[0, :])
t_theta = numerix.array([theta, Tout]).T
t_theta = t_theta[t_theta[:, 0].argsort()]
h5f = h5py.File("fuel_clad.h5", 'w')
h5f.create_group("inside_surf")
h5f.create_group("outside_surf")
h5f.create_dataset("outside_surf/r_T", data=t_theta)
h5f.close()

#############
# View Data #
#############
pl.figure(10)
pl.plot(t_theta[:, 0], t_theta[:, 1], label="Fuel - Outer Surface T")
pl.legend()
pl.xlabel("Azimuth [rad]")
pl.ylabel("Temperature [K]")
pl.savefig("outer_T_fuel.png")

viewer = Viewer(vars=T)
viewer.plot("2D_fuel_clad.png")
raw_input("Press <return> to exit...")
