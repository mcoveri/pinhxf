#!/usr/bin/python2
import h5py
import pylab as pl
import matplotlib.pyplot as plt
import seaborn as sns


def main():
    clad_only_h5 = h5py.File("clad_only.h5")
    fuel_clad_h5 = h5py.File("fuel_clad.h5")
    pl.figure()
    fig, ax = plt.subplots(1)
    ax.plot(clad_only_h5["outside_surf/r_T"][:, 0], clad_only_h5["outside_surf/r_T"][:, 1], label="Clad-Only Surface T")
    ax.plot(fuel_clad_h5["outside_surf/r_T"][:, 0], fuel_clad_h5["outside_surf/r_T"][:, 1], label="Full Model Surface T")
    ax.legend(loc=1)
    #
    ax2 = ax.twinx()
    abs_diff = - clad_only_h5["outside_surf/r_T"][:, 1] + fuel_clad_h5["outside_surf/r_T"][:, 1]
    print("MAX abs diff = " + str(max(abs(abs_diff))))
    ax2.plot(fuel_clad_h5["outside_surf/r_T"][:, 0], abs_diff, sns.xkcd_rgb["pale red"], label="Absolute Diff [K]")
    ax2.grid(b=False)
    ax2.legend(loc=4)
    pl.xlabel("Azimuth [rad]")
    pl.ylabel("Temperature [K]")
    pl.savefig("model_compare.png")
    clad_only_h5.close()
    fuel_clad_h5.close()

if __name__ == "__main__":
    main()
